# Dojo de Testes Unitários

Este projeto contém o estado final do dojo e testes unitários dos treinamentos **Certified Scrum Developer** e **Testes Automatizados** da Knowledge21.

O pipeline estaria configurado para subir o container na AWS, mas a gente falhou miseravelmente em deixar isso simples, e por isso ta subindo numa VM na digital ocean mesmo, com uma gambetinha por SSH. Sorry. Aceitamos pull requests que subam na Amazon de uma maneira simples. Passar bem.

## Requisitos

O projeto foi criado usando o Python 3.7.2, porém o estado inicial funciona em qualquer versão do Python 3

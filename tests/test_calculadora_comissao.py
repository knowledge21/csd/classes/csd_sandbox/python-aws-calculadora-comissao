from unittest import TestCase
import sistema_vendas.calculadora_comissao as calculadora_comissao


class TestCalculadoraComissao(TestCase):
    def test_vendamenor10k_vendade100reais(self):
        valor_venda = 100
        resultado = 5
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)

    def test_vendamaior10k_vendade20milreais(self):
        valor_venda = 20000
        resultado = 1200
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)


    def test_vendamaior10k_vendade30milreais(self):
        valor_venda = 30000
        resultado = 1800
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)

    def test_vendamenor10k_vendade200reais(self):
        valor_venda = 200
        resultado = 10
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)

    def test_vendamaiorr10kcomcasadecimal_vendade10055_59reais(self):
        valor_venda = 10055.59
        resultado = 603.33
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)

    def test_vendamaior10k_vendade10001reais(self):
        valor_venda = 10001
        resultado = 600.06
        comissao = calculadora_comissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)
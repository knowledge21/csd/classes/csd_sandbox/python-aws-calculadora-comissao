FROM python:3.7.4-alpine3.10

COPY . .

RUN pip install flask gunicorn

CMD gunicorn -b 0.0.0.0:8000 sistema_vendas.app:app